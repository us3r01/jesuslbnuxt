const colors = require('vuetify/es5/util/colors').default

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
 head: {
  htmlAttrs: {
    lang: 'es'
  },
},
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  /*
  ** Plugins to load before mounting the App
  */
 plugins: [
  {src: '~plugins/components', mode: 'client' },
  '~/plugins/jsonld',
  '~/plugins/disqus',
  { src: '@/plugins/vue-mavon-editor', srr: false },
  {src: '@/plugins/aos.js', mode: 'client'}
],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
 markdownit: {
  preset: 'default',
  injected: true,
  linkify: true,
  html: true,
  breaks: true,
  use: [
    'markdown-it-div',
    'markdown-it-attrs'
  ]
}
  ,
  modules: [
    '@nuxtjs/sitemap',
    '@nuxtjs/markdownit',
    '@nuxtjs/google-adsense',
    ['@nuxtjs/firebase',  
    {
      config: {
        apiKey: process.env.API_KEY,
        authDomain: process.env.AUTH_DOMAIN,
        databaseURL: process.env.DATABASE_URL,
        projectId: process.env.PROJECT_ID,
        storageBucket: process.env.STORAGE_BUCKET,
        messagingSenderId: process.env.MESSAGING_SENDER,
        appId: process.env.APP_ID,
        measurementId: process.env.MEASUREMENT_ID
      },
      services: {

        firestore: true,
        functions: {
          location: 'us-central1', // Default
          emulatorPort: 12345
        },
        storage: true,
        realtimeDb: true,
        performance: true,
        analytics: true,
        remoteConfig: {
          settings: {
            fetchTimeoutMillis: 60000, // Default
            minimumFetchIntervalMillis: 43200000 // Default
          },
          defaultConfig: {
            welcome_message: "Welcome"
          }
        },
        messaging: {
          createServiceWorker: true
        }
    }
  }
    ]
  ],
  'google-adsense': {
    id: 'ca-pub-4017969152268632'
  },
  sitemap: {
    hostname: 'https://wwww.jesuslb.com',
    gzip: true,
    exclude: [
      '/admin'
    ],
    routes: [
      '/',
      '/contacto',
      '/seo',
      '/paginasweb',
      '/portafolio',
      '/blog/seo-con-vue-spa-+-material-design-+-firebase---caso-de-exito-IC64RldsWvLFJHS9IQW9',
      '/blog/cursos-presenciales-de-ethereum-solidity-y-web3js-S7HOzR1AAgBiZDl1Sj8Q',
      '/blog/por-que-es-importante-tener-una-pagina-web-onEBHl6yCMKHvMDAArGA',
      '/blog/the-cat-shop-tutorial-solidity-and-vuejs-SkMDc1NxVTVFa909wngQ',
      '/blog/tutorial-de-una-tienda-de-gatitos-con-solidity-y-vue-jx8WBxdds5ykcO3oXQ4h'
    ]
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: colors.blue,
          secondary: colors.grey.darken1,
          accent: colors.shades.black,
          error: colors.red.accent3,
        },
      }
    }
  },
  /*
  ** Build configuration
  */
 buildDir: 'nuxt',
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  server: {port: 3100}
}
