import firebase from 'firebase/app'

  /** --------------------------------------------------------------------------------------------- **/
  /** -------------------------------------- END: Import Scripts ---------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

export default async (ctx, inject) => {
  const options = {"config":{"apiKey":"AIzaSyDxu5zvfwJBuXS1rk7CyZ72gE2rxbmCx9A","authDomain":"jesuslbweb.firebaseapp.com","databaseURL":"https:\u002F\u002Fjesuslbweb.firebaseio.com","projectId":"jesuslbweb","storageBucket":"jesuslbweb.appspot.com","messagingSenderId":"444832936422","appId":"1:444832936422:web:b9471ea575a0f53e0ae627","measurementId":"G-L561WBNNZT"},"services":{"firestore":true,"functions":{"location":"us-central1","emulatorPort":12345},"storage":true,"realtimeDb":true,"performance":true,"analytics":true,"remoteConfig":{"settings":{"fetchTimeoutMillis":60000,"minimumFetchIntervalMillis":43200000},"defaultConfig":{"welcome_message":"Welcome"}},"messaging":{"createServiceWorker":true}}}
  const firebaseConfig = options.config

  // Don't include when Firebase is already initialized
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }

  /** --------------------------------------------------------------------------------------------- **/
  /** -------------------------------------- FIREBASE AUTH ---------------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  /** --------------------------------------------------------------------------------------------- **/
  /** -------------------------------------- FIREBASE REALTIME DB --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

    await import(/* webpackChunkName: 'firebase-database' */'firebase/database')

    const fireDb = firebase.database()
    const fireDbObj = firebase.database
    inject('fireDb', fireDb)
    inject('fireDbObj', fireDbObj)

  /** --------------------------------------------------------------------------------------------- **/
  /** ---------------------------------------- FIREBASE FIRESTORE --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

    await import(/* webpackChunkName: 'firebase-firestore' */'firebase/firestore')

    const fireStore = firebase.firestore()
    const fireStoreObj = firebase.firestore
    inject('fireStore', fireStore)
    inject('fireStoreObj', fireStoreObj)

    const settings = options.services.firestore.settings
    if (settings) {
      fireStore.settings(settings)
    }

    const enablePersistence = options.services.firestore.enablePersistence
    if (enablePersistence && process.client) {
      try {
        await fireStore.enablePersistence((
          typeof enablePersistence === 'object'
            ? enablePersistence
            : {}
        ))
      } catch (err) {
        if (err.code == 'failed-precondition') {
          console.info("Firestore Persistence not enabled. Multiple tabs open, persistence can only be enabled in one tab at a a time.")
        } else if (err.code == 'unimplemented') {
          console.info("Firestore Persistence not enabled. The current browser does not support all of the features required to enable persistence.")
        }
      }
    }

  /** --------------------------------------------------------------------------------------------- **/
  /** ------------------------------------------ FIREBASE STORAGE --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

    await import(/* webpackChunkName: 'firebase-storage' */'firebase/storage')

    const fireStorage = firebase.storage()
    const fireStorageObj = firebase.storage
    inject('fireStorage', fireStorage)
    inject('fireStorageObj', fireStorageObj)

  /** --------------------------------------------------------------------------------------------- **/
  /** ---------------------------------------- FIREBASE FUNCTIONS --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

    await import(/* webpackChunkName: 'firebase-functions' */'firebase/functions')

    // If .location is undefined, default will be "us-central1"
    const fireFunc = firebase.app().functions(options.services.functions.location)
    const fireFuncObj = firebase.functions

    // Uses emulator, if emulatorPort is set.
    if (options.services.functions.emulatorPort) {
      fireFunc.useFunctionsEmulator(`http://localhost:${options.services.functions.emulatorPort}`)
    }

    inject('fireFunc', fireFunc)
    inject('fireFuncObj', fireFuncObj)

  /** --------------------------------------------------------------------------------------------- **/
  /** ---------------------------------------- FIREBASE MESSAGING --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Messaging can only be initiated on client side
  if (process.browser) {
    await import(/* webpackChunkName: 'firebase-messaging' */'firebase/messaging')

    if (firebase.messaging.isSupported()) {
      const fireMess = firebase.messaging()
      const fireMessObj = firebase.messaging

      if (firebaseConfig.fcmPublicVapidKey) {
        fireMess.usePublicVapidKey(firebaseConfig.fcmPublicVapidKey)
      }

      inject('fireMess', fireMess)
      inject('fireMessObj', fireMessObj)
    }
  }

  /** --------------------------------------------------------------------------------------------- **/
  /** -------------------------------------- FIREBASE REALTIME DB --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Performance can only be initiated on client side

  if(process.browser) {
    await import(/* webpackChunkName: 'firebase-performance' */'firebase/performance')

    const firePerf = firebase.performance()
    const firePerfObj = firebase.performance
    inject('firePerf', firePerf)
    inject('firePerfObj', firePerfObj)
  }

  /** --------------------------------------------------------------------------------------------- **/
  /** ---------------------------------------- FIREBASE ANALYTICS --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Analytics can only be initiated on the client side

  if (process.browser) {
    await import(/* webpackChunkName: 'firebase-analytics' */'firebase/analytics')

    const fireAnalytics = firebase.analytics()
    const fireAnalyticsObj = firebase.analytics
    inject('fireAnalytics', fireAnalytics)
    inject('fireAnalyticsObj', fireAnalyticsObj)
  }

  /** --------------------------------------------------------------------------------------------- **/
  /** --------------------------------- FIREBASE REMOTE CONFIG DB --------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Remote Config can only be initiated on the client side
  if (process.browser) {
    await import(/* webpackChunkName: 'firebase-remote-config' */'firebase/remote-config')

    const fireConfig = firebase.remoteConfig()
    const fireConfigObj = firebase.remoteConfig

    const { settings: remoteSettings, defaultConfig: remoteDefaultConfig } = options.services.remoteConfig
    if (remoteSettings) {
      const { minimumFetchIntervalMillis, fetchTimeoutMillis } = remoteSettings
      fireConfig.settings = {
        fetchTimeoutMillis: fetchTimeoutMillis ? fetchTimeoutMillis : 60000,
        minimumFetchIntervalMillis: minimumFetchIntervalMillis ? minimumFetchIntervalMillis : 43200000
      }
    }
    fireConfig.defaultConfig = (remoteDefaultConfig)

    inject('fireConfig', fireConfig)
    inject('fireConfigObj', fireConfigObj)
  }
}
