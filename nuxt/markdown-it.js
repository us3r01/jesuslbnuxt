import MarkdownIt from 'markdown-it'

const handlePlugin = (plugin) => plugin.default || plugin

export default ({ app }, inject) => {
  const md = new MarkdownIt('default', {"linkify":true,"html":true,"breaks":true})

  md.use(handlePlugin(require('markdown-it-div')))

  md.use(handlePlugin(require('markdown-it-attrs')))

  inject('md', md)
}
