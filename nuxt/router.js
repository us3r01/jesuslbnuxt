import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _f8ed264c = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _44d61e0a = () => interopDefault(import('../pages/blog.vue' /* webpackChunkName: "pages/blog" */))
const _23e456b5 = () => interopDefault(import('../pages/blog/_id.vue' /* webpackChunkName: "pages/blog/_id" */))
const _23831899 = () => interopDefault(import('../pages/blogPosts.vue' /* webpackChunkName: "pages/blogPosts" */))
const _ef91e4ea = () => interopDefault(import('../pages/Cards.vue' /* webpackChunkName: "pages/Cards" */))
const _7255897a = () => interopDefault(import('../pages/contacto/index.vue' /* webpackChunkName: "pages/contacto/index" */))
const _69e1c5b2 = () => interopDefault(import('../pages/paginasweb/index.vue' /* webpackChunkName: "pages/paginasweb/index" */))
const _3ec49308 = () => interopDefault(import('../pages/pitch.vue' /* webpackChunkName: "pages/pitch" */))
const _721f2c58 = () => interopDefault(import('../pages/portafolio/index.vue' /* webpackChunkName: "pages/portafolio/index" */))
const _e9c506da = () => interopDefault(import('../pages/Proceso.vue' /* webpackChunkName: "pages/Proceso" */))
const _743c7a19 = () => interopDefault(import('../pages/PuenteFondo.vue' /* webpackChunkName: "pages/PuenteFondo" */))
const _34483868 = () => interopDefault(import('../pages/seo/index.vue' /* webpackChunkName: "pages/seo/index" */))
const _34667de6 = () => interopDefault(import('../pages/signin/index.vue' /* webpackChunkName: "pages/signin/index" */))
const _78e88f46 = () => interopDefault(import('../pages/paginasweb/tituloPags.vue' /* webpackChunkName: "pages/paginasweb/tituloPags" */))
const _89657286 = () => interopDefault(import('../pages/paginasweb/ventajaspags.vue' /* webpackChunkName: "pages/paginasweb/ventajaspags" */))
const _910d0748 = () => interopDefault(import('../pages/portafolio/portafoliocards.vue' /* webpackChunkName: "pages/portafolio/portafoliocards" */))
const _239a40cc = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin",
    component: _f8ed264c,
    name: "admin"
  }, {
    path: "/blog",
    component: _44d61e0a,
    name: "blog",
    children: [{
      path: ":id?",
      component: _23e456b5,
      name: "blog-id"
    }]
  }, {
    path: "/blogPosts",
    component: _23831899,
    name: "blogPosts"
  }, {
    path: "/Cards",
    component: _ef91e4ea,
    name: "Cards"
  }, {
    path: "/contacto",
    component: _7255897a,
    name: "contacto"
  }, {
    path: "/paginasweb",
    component: _69e1c5b2,
    name: "paginasweb"
  }, {
    path: "/pitch",
    component: _3ec49308,
    name: "pitch"
  }, {
    path: "/portafolio",
    component: _721f2c58,
    name: "portafolio"
  }, {
    path: "/Proceso",
    component: _e9c506da,
    name: "Proceso"
  }, {
    path: "/PuenteFondo",
    component: _743c7a19,
    name: "PuenteFondo"
  }, {
    path: "/seo",
    component: _34483868,
    name: "seo"
  }, {
    path: "/signin",
    component: _34667de6,
    name: "signin"
  }, {
    path: "/paginasweb/tituloPags",
    component: _78e88f46,
    name: "paginasweb-tituloPags"
  }, {
    path: "/paginasweb/ventajaspags",
    component: _89657286,
    name: "paginasweb-ventajaspags"
  }, {
    path: "/portafolio/portafoliocards",
    component: _910d0748,
    name: "portafolio-portafoliocards"
  }, {
    path: "/",
    component: _239a40cc,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
